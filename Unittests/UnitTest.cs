// <copyright file="UnitTest.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using Monosoft.Service.PasswordGenerator.V1;
    using Monosoft.Service.PasswordGenerator.V1.Commands;
    using NUnit.Framework;

    /// <summary>
    /// UnitTests.
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
    }
}