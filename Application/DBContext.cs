// <copyright file="DBContext.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.PasswordGenerator.V1
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Config;
    using ServiceStack.OrmLite;
    using ServiceStack.OrmLite.Legacy;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Database.
    /// </summary>
    public class DBContext
    {
        private static string dbName = "passwordgenerator";
        private static OrmLiteConnectionFactory dbFactory = null;

        private static DBContext _dBContext = null;

        /// <summary>
        /// Gets db.
        /// </summary>
        public static DBContext instance
        {
            get
            {
                if (_dBContext == null)
                {
                    _dBContext = new DBContext(GlobalRessources.getConfig().GetSetting<SQLSettings>());
                }
                return _dBContext;
            }
            set { _dBContext = value; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DBContext"/> class.
        /// </summary>
        /// <param name="settings">SQLSettings settings.</param>
        public DBContext(SQLSettings settings)
        {
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<Database.ContextInfo>();
                db.CreateTableIfNotExists<Database.Key>();
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServer2016Dialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }
        /// <summary>
        /// Context.
        /// </summary>
        /// <param name="input">Monosoft.Service.PasswordGenerator.V1.DTO.ContextInsert input.</param>
        public void Context(DTO.ContextInsert input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Database.ContextInfo>(input.Id);
                var res = db.Single<Database.ContextInfo>(x => x.Context == input.Context);

                Database.ContextInfo context = new Database.ContextInfo();
                context.Id = input.Id;
                context.Context = input.Context;
                context.PreSalt = Guid.NewGuid().ToString();
                context.PostSalt = Guid.NewGuid().ToString();

                if (dbresult == null && res == null)
                {
                    db.Insert<Database.ContextInfo>(context);
                }
                else
                {
                    throw new ElementAlreadyExistException("item already exists", context.Context);
                }
            }
        }

        /// <summary>
        /// Keys.
        /// </summary>
        /// <param name="input">Monosoft.Service.PasswordGenerator.V1.DTO.ContextInsert input.</param>
        public void Key(Database.Key input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Database.Key>(input.Id);

                if (dbresult == null)
                {
                    db.Insert<Database.Key>(input);
                }
            }
        }

        /// <summary>
        /// GetKeys.
        /// </summary>
        /// <param name="input">Monosoft.Service.PasswordGenerator.V1.DTO.ContextInsert input.</param>
        public List<DTO.Key> GetKeys(Database.ContextInfo input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<Database.Key>(x => x.ContextId == input.Id);

                if (dbresult == null)
                {
                    throw new ElementDoesNotExistException("item do not exists", input.Id.ToString());
                }
                List<DTO.Key> keys = new List<DTO.Key>();
                foreach (var item in dbresult)
                {
                    DTO.Key key = new DTO.Key();
                    key.Id = item.Id;
                    key.Name = item.Name;
                    keys.Add(key);
                }
                return keys;
            }
        }

        public Database.ContextInfo GetContext(DTO.PasswordGenerator input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.Single<Database.ContextInfo>(x => x.Context == input.Context);


                if (dbresult == null)
                {
                    throw new ElementDoesNotExistException("item do not exists", input.Context);
                }
                return dbresult;
            }
        }
    }
}