﻿// <copyright file="DBContext.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.PasswordGenerator.V1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    public class Logic
    {
        public DTO.PasswordGeneratorResponse GeneratorPassword(DTO.PasswordGenerator input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            var context = DBContext.instance.GetContext(input);
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            var hashPassword = new DTO.PasswordGeneratorResponse();
            var password = context.PreSalt + input.Name + context.PostSalt;

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                hashPassword.Password = builder.ToString();
            }

            Database.Key key = new Database.Key();
            key.Id = Guid.NewGuid();
            key.Name = input.Name;
            key.ContextId = context.Id;

            DBContext.instance.Key(key);
            return hashPassword;
        }
    }
}
