﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.Database
{
    public class ContextInfo
    {
        public Guid Id { get; set; }
        public string Context { get; set; }
        public string PreSalt { get; set; }
        public string PostSalt { get; set; }
    }
}
