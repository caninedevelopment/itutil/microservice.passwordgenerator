﻿namespace Monosoft.Service.PasswordGenerator
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new V1.Commands.Namespace(),
            });
        }
    }
}
