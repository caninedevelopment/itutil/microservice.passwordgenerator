﻿// <copyright file="DBContext.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.PasswordGenerator.V1.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Namespace : BaseNamespace
    {
        private const string Servicename = "PasswordGenerator";

        /// <summary>
        /// Gets Claims.
        /// </summary>
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator") }, key = "Monosoft.Service.PasswordGenerator.isAdmin" };

        public Namespace() : base("PasswordGenerator", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Insert(),
                new GetKeysFromContext(),
                new Generate()
            });
        }
    }
}
