﻿using ITUtil.Common.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.Commands
{
    public class GetKeysFromContext : GetCommand<DTO.PasswordGenerator, List<DTO.Key>>
    {
        public GetKeysFromContext() : base("Get all keys from context")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">object input.</param>
        /// <returns>List DTO.Fragment.</returns>
        public override List<DTO.Key> Execute(DTO.PasswordGenerator input)
        {
            var context = DBContext.instance.GetContext(input);
      
            return DBContext.instance.GetKeys(context);
        }
    }
}
