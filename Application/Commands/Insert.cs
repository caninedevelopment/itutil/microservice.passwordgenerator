﻿using Google.Protobuf.WellKnownTypes;
using ITUtil.Common.Command;
using Monosoft.Service.PasswordGenerator.V1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.Commands
{
    public class Insert : InsertCommand<ContextInsert>
    {
        public Insert() : base("Insert context if context is not null or empty")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">object input.</param>
        /// <returns>List DTO.Fragment.</returns>
        public override void Execute(DTO.ContextInsert input)
        {
            if (input.Id == null)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");

            }
            if (string.IsNullOrEmpty(input.Context))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Context was not provided", "Context");
            }

            DBContext.instance.Context(input);
        }
    }
}
