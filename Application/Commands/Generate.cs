﻿using ITUtil.Common.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.Commands
{
    public class Generate : GetCommand<DTO.PasswordGenerator, DTO.PasswordGeneratorResponse>
    {
        public Generate() : base("Generate Password if context and name is provided and if context exist")
        {
            this.claims.Add(Namespace.IsAdmin);
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">DTO.PasswordGenerator input.</param>
        /// <returns>List DTO.PasswordGeneratorResponse.</returns>
        public override DTO.PasswordGeneratorResponse Execute(DTO.PasswordGenerator input)
        {
            if (string.IsNullOrEmpty(input.Context))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Context was not provided", "Context");
            }
            if (string.IsNullOrEmpty(input.Name))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Name was not provided", "Name");
            }

            Logic logic = new Logic();
            var res = logic.GeneratorPassword(input);
            return res;
        }
    }
}
