﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.DTO
{
    public class Key
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
