﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.DTO
{
    public class PasswordGenerator
    {
        public string Context { get; set; }
        public string Name { get; set; }
    }
}
