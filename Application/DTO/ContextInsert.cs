﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.DTO
{
    public class ContextInsert
    {
        public Guid Id { get; set; }
        public string Context { get; set; }
    }
}
