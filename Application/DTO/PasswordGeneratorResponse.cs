﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.DTO
{
    public class PasswordGeneratorResponse
    {
        public string Password { get; set; }
    }
}
