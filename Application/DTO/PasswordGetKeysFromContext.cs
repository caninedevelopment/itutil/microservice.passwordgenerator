﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.PasswordGenerator.V1.DTO
{
    public class PasswordGetKeysFromContext
    {
        public List<DTO.Key> AllKeys { get; set; }
    }
}
